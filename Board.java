public class Board {
	
	private Square[][] tictactoeBoard;
	
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		
		for (int row = 0; row<3; row++) {
			for (int col=0; col<3; col++) {
				this.tictactoeBoard[row][col] = Square.BLANK;
			}
		}
	}
	
	public String toString() {
		String board = "  0 1 2\n";
		
		for (int row = 0; row<3; row++) {
			board += row + " ";
			for (int col=0; col<3; col++) {
				board += this.tictactoeBoard[row][col] + " ";
			}
			board += "\n";
		}
		return board;
	}
	
	/*public static void main(String[] args) {
		Board brd = new Board();
		System.out.println(brd.toString());
		System.out.println(checkIfWinningHorizontal(playerToken));
	}*/
	
	
	public boolean placeToken(int row, int col, Square playerToken) {
		boolean token = false;
		if (row >0 || col <2 || row <2 || col >0) {
			token = true;
		}
		else {
			token = false;
		}
		
		if (tictactoeBoard[row][col] == Square.BLANK) {
			tictactoeBoard[row][col] = playerToken;
			token = true;
		}
		else {
			token = false;
		}
		return token;
	}
	
	public boolean checkIfFull() {
		boolean checkiffull = true;
		for (int row=0; row<3; row++) {
			for (int col=0; col<3; col++) {
				if (this.tictactoeBoard[row][col] == Square.BLANK) {
					checkiffull = false;
				}
			}
		}
		return checkiffull;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) {
		boolean horizontalWin = false;
		int count = 0;
		for (int row=0; row<3; row++) {
			for (int col=0; col<3; col++) {
				if (this.tictactoeBoard[0][col] == playerToken) {
					count++;
					if (count == 3) {
						horizontalWin = true;
						count = 0;
					}
				}
				else if (this.tictactoeBoard[1][col] == playerToken) {
					count++;
					if (count == 3) {
						horizontalWin = true;
						count = 0;
					}
				}
				else if (this.tictactoeBoard[2][col] == playerToken) {
					count++;
					if (count == 3) {
						horizontalWin = true;
						count = 0;
					}
				}
				else {
					return false;
				}
			}
		}
		return horizontalWin;
	}
	
	private boolean checkIfWinningVertical(Square playerToken) {
		boolean verticalWin = false;
		int count = 0;
		for (int row=0; row<3; row++) {
			for (int col=0; col<3; col++) {
				if (this.tictactoeBoard[row][0] == playerToken) {
					count++;
					if (count == 3) {
						verticalWin = true;
						count = 0;
					}
				}
				else if (this.tictactoeBoard[row][1] == playerToken) {
					count++;
					if (count == 3) {
						verticalWin = true;
						count = 0;
					}
				}
				else if (this.tictactoeBoard[row][2] == playerToken) {
					count++;
					if (count == 3) {
						verticalWin = true;
						count = 0;
					}
				}
				else {
					return false;
				}
			}
		}
		return verticalWin;
	}
	
	public boolean checkIfWinning(Square playerToken) {
		if (checkIfWinningVertical(playerToken) == true || checkIfWinningHorizontal(playerToken) == true) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*public boolean checkIfWinningDiagonal(Square PlayerToken) {
		
	}*/
}