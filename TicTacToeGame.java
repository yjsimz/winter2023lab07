import java.util.Scanner;

public class TicTacToeGame {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to Tic-Tac-Toe Game!");
		
		Board board = new Board();
		Boolean gameOver = false;
		int player = 1;
		Square playerToken;
		
		System.out.println("Player 1's token: X");	
		System.out.println("Player 2's token: O\n");	
		
		while (gameOver == false) {
			System.out.println(board.toString());
			if (player == 1) {
				playerToken = Square.X;
			}
			else {
				playerToken = Square.O;
			}
			
			System.out.println("Player " + player + ": it's your turn. Where do you want to place your token?");	
			int row = scan.nextInt();
			int col = scan.nextInt();
			
			while (board.placeToken(row, col, playerToken) == false) {	
			
				System.out.println(board.toString());
				System.out.println("Please enter valid indexes");	
				row = scan.nextInt();
				col = scan.nextInt();
			}
			
			if (board.checkIfFull() == true) {
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else if (board.checkIfWinning(playerToken) == true) {
				System.out.println(board.toString());
				System.out.println("Player " + player + " wins!");
				gameOver = true;
			}
			else {
				if (player != 1) {
					player = 1;
					System.out.println(player);
				}
				else{
					player++;
				}
			}
			
		}
	}
}